# ftoken

==

Package ftoken is a faster subset of the stdlib go/token package.

Installation

    $ go get modernc.org/ftoken

Documentation: [godoc.org/modernc.org/browse/internal/ftoken](http://godoc.org/modernc.org/browse/internal/ftoken)
