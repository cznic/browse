# browse

Terminal Go Source Code Browser

Installation

    $ go get [-u] modernc.org/browse

Documentation: [godoc.org/modernc.org/browse](http://godoc.org/modernc.org/browse)

----

Screenshot

![](https://modernc.org/browse/blob/images/browse.png)
